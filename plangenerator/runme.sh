#!/usr/bin/env bash

DIR=$(pwd)

dockerun(){
    echo --------------------------------------
    echo --------------------------------------
    echo Stop the running image vishwas/plangenerator
    echo --------------------------------------
    echo --------------------------------------
    sleep 2
    docker stop $(docker ps -q --filter ancestor=vishwas/plangenerator )

    echo --------------------------------------
    echo --------------------------------------
    echo ---Remove the image vishwas/plangenerator---
    echo --------------------------------------
    echo --------------------------------------
    sleep 2
    docker rmi -f vishwas/plangenerator

    echo --------------------------------------
    echo --------------------------------------
    echo ---Build the image vishwas/plangenerator ---
    echo --------------------------------------
    echo --------------------------------------
    $DIR/mvnw install dockerfile:build

    echo --------------------------------------
    echo --------------------------------------
    echo -------Run the build image------------
    echo --------------------------------------
    echo --------------------------------------
    sleep 2
    docker run -e "SPRING_PROFILES_ACTIVE=prod" -p 8080:8080 -t vishwas/plangenerator
}

bootrun(){
    echo --------------------------------------
    echo -------------Boot Run-----------------
    echo --------------------------------------
    $DIR/mvnw spring-boot:run
}

stopdocker(){
    echo --------------------------------------
    echo --------------------------------------
    echo Stopping the running image vishwas/plangenerator
    echo --------------------------------------
    echo --------------------------------------
    docker stop $(docker ps -q --filter ancestor=vishwas/plangenerator )
}

OPTION=$1

case $OPTION in
dockerun)
    echo "Starting as docker"
    dockerun
    ;;
stopdocker)
    echo "Stop docker"
    stopdocker
    ;;
bootrun)
    echo "Sarting as bootrun"
    bootrun
    ;;
*)
    echo "***********************************************************************************************"
    echo "-- Options --"
    echo "*  ./runme.sh dockerun                     - dockerun : Starting as docker."
    echo "*  ./runme.sh bootrun                      - bootrun : Sarting as bootrun. mvnw spring-boot:run"
    echo "*  ./runme.sh stopdocker                   - stopdocker : Stop docker. "
    echo "***********************************************************************************************"

esac
