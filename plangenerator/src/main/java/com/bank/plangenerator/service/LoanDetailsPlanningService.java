package com.bank.plangenerator.service;

import com.bank.plangenerator.domainobjects.LoanDetails;
import com.bank.plangenerator.domainobjects.PaymentHistory;

import java.util.List;

public interface LoanDetailsPlanningService {
    List<PaymentHistory> generatePlan(LoanDetails loanDetails);
}
