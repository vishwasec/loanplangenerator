#PlanGenerator
To generate plan for a given loan.

##To Run

```
./runme.sh dockerun     - dockerun : Starting as docker.[Currently doest work with gateway as docker localhost is different from eureka localhost.] 
./runme.sh bootrun                      - bootrun : Sarting as bootrun. mvnw spring-boot:run
./runme.sh stopdocker                   - stopdocker : Stop docker.
```

You can check actuator for status of app.

```
http://localhost:8080/actuator/info
http://localhost:8080/actuator/health
http://localhost:8080/actuator/metrics
```


## About api's 

Once plangenerator is started you can check swagger for complete details and to try it directly.

Swagger url :  **http://localhost:8080/swagger-ui.html**

**POST /generate-plan**

Generates plan for the given input loandetails. Not the output is cached, so next call with the same would be much faster. TTL for cache is 10 mins, it can be configured 

To **call directly** to the service,
```
curl -X POST \
  http://localhost:8080/generate-plan \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -d '{
"loanAmount": "5000",
"nominalRate": "5.0",
"duration": 24,
"startDate": "2018-01-01T00:00:01Z"
}'

```

To call through **Gateway**, 

**Note initial calls would fail as it takes time for the services to register to eureka** 

```
curl -X POST \
  http://localhost:9300/generate-plan \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:9300' \
  -d '{
"loanAmount": "5000",
"nominalRate": "5.0",
"duration": 24,
"startDate": "2018-01-01T00:00:01Z"
}'

```

Payload
```
{
"loanAmount": "5000",
"nominalRate": "5.0",
"duration": 24,
"startDate": "2018-01-01T00:00:01Z"
}
```

Response
```
[
    {
        "borrowerPaymentAmount": "219.36",
        "date": "2018-01-01T00:00:01Z",
        "initialOutstandingPrincipal": "5000.0",
        "interest": "20.83",
        "principal": "198.53",
        "remainingOutstandingPrincipal": "4801.47"
    },
    
    ...
    
    {
            "borrowerPaymentAmount": "219.28",
            "date": "2019-12-01T00:00:01Z",
            "initialOutstandingPrincipal": "218.37",
            "interest": "0.91",
            "principal": "218.45",
            "remainingOutstandingPrincipal": "0.0"
        }
    ]
```

## Improvments 

Authorization  service is missing, didnt get time to add this.

Docker for Eureka and Gateway. This would have simplified the startup process.

