package com.bank.plangenerator.exceptions;

public class InvalidInputException extends RuntimeException {

    public InvalidInputException(String error) {
        super(error);
    }
}
