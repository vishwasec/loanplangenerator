package com.bank.plangenerator.domainobjects;


import java.time.ZonedDateTime;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@EqualsAndHashCode()
@ToString
public class LoanDetails {

    private Double loanAmount;
    private double nominalRate;
    private Integer duration;
    private ZonedDateTime startDate;

}
