package com.bank.plangenerator.config;

import com.google.common.cache.CacheBuilder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheConfig {

    @Value("${cache.ttl:5}")
    private Integer cacheTTL;

    @Bean
    public Cache customExpiredLoandetailsplanKeyCache() {
        return new ConcurrentMapCache("loandetailsplan", CacheBuilder.newBuilder()
                .expireAfterWrite(cacheTTL, TimeUnit.MINUTES).maximumSize(100).build().asMap(), false);
    }

}
