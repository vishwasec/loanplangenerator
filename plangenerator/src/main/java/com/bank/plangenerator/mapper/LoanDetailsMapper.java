package com.bank.plangenerator.mapper;

import com.bank.plangenerator.controller.dto.LoanDetailsRequestDto;
import com.bank.plangenerator.controller.dto.LoanDetailsResponseDto;
import com.bank.plangenerator.domainobjects.LoanDetails;
import com.bank.plangenerator.domainobjects.PaymentHistory;

import static com.bank.plangenerator.util.NumberUtils.doubleToString;
import static java.lang.Double.parseDouble;
import static java.time.ZonedDateTime.parse;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

public class LoanDetailsMapper {

    public static LoanDetails toLoanDetails(LoanDetailsRequestDto requestDto) {

        return LoanDetails.builder().duration(requestDto.getDuration())
                .loanAmount(parseDouble(requestDto.getLoanAmount()))
                .nominalRate(parseDouble(requestDto.getNominalRate()))
                .startDate(parse(requestDto.getStartDate(), ISO_DATE_TIME)).build();
    }

    public static LoanDetailsResponseDto toLoanDetailsResponseDto(PaymentHistory paymentHistory) {
        return LoanDetailsResponseDto.builder()
                .borrowerPaymentAmount(doubleToString(paymentHistory.getBorrowerPaymentAmount()))
                .initialOutstandingPrincipal(doubleToString(paymentHistory.getInitialOutstandingPrincipal()))
                .interest(doubleToString(paymentHistory.getInterest()))
                .principal(doubleToString(paymentHistory.getPrincipal()))
                .remainingOutstandingPrincipal(doubleToString(paymentHistory.getRemainingOutstandingPrincipal()))
                .date(paymentHistory.getDate()).build();
    }

}
