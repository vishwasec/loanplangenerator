package com.bank.plangenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableCaching
@EnableDiscoveryClient
public class PlangeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlangeneratorApplication.class, args);
    }

}
