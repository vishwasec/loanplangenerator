package com.bank.plangenerator.service.impl;

import com.bank.plangenerator.domainobjects.LoanDetails;
import com.bank.plangenerator.domainobjects.PaymentHistory;
import com.bank.plangenerator.service.LoanDetailsPlanningService;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import static com.bank.plangenerator.util.LoanCalculatorUtil.getBorrowerPaymentAmount;
import static com.bank.plangenerator.util.LoanCalculatorUtil.getInterest;
import static com.bank.plangenerator.util.NumberUtils.doubleLessThanZero;
import static com.bank.plangenerator.util.NumberUtils.subDouble;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;

@Service
@Slf4j
public class LoanDetailsPlanningServiceImpl implements LoanDetailsPlanningService {

    private static final int MONTHS_TO_ADD = 1;
    private static final int RETURN_VALUE = 0;

    @Override
    @Cacheable(cacheNames = "loandetailsplan", key = "#loanDetails")
    public List<PaymentHistory> generatePlan(LoanDetails loanDetails) {

        log.info("Generating plan for {}", loanDetails);
        double interest, principal;
        List<PaymentHistory> loanDetailsRespons = new ArrayList<>();

        ZonedDateTime localDate = loanDetails.getStartDate();
        double remainingOutstandingPrincipal = loanDetails.getLoanAmount();
        double borrowerPaymentAmount = getBorrowerPaymentAmount(loanDetails);
        //Can also be calculated with recursions.
        for (int i = 0; i < loanDetails.getDuration(); i++) {

            interest = getInterest(loanDetails.getNominalRate(), remainingOutstandingPrincipal);
            principal = subDouble(borrowerPaymentAmount, interest);
            remainingOutstandingPrincipal = subDouble(remainingOutstandingPrincipal, principal);

            //Save loan plan.
            PaymentHistory paymentHistoryDto = PaymentHistory.builder()
                    .interest(interest).principal(principal).date(ISO_INSTANT.format(localDate))
                    .initialOutstandingPrincipal(remainingOutstandingPrincipal + principal)
                    .borrowerPaymentAmount(doubleLessThanZero(remainingOutstandingPrincipal,// for final month we reduce the amount to be paid.
                            subDouble(borrowerPaymentAmount, remainingOutstandingPrincipal), borrowerPaymentAmount))
                    .remainingOutstandingPrincipal(doubleLessThanZero(remainingOutstandingPrincipal, RETURN_VALUE, remainingOutstandingPrincipal))//for final month make it 0
                    .build();

            loanDetailsRespons.add(paymentHistoryDto);
            localDate = localDate.plusMonths(MONTHS_TO_ADD); // increment by month
        }

        log.info("Completed plan generation successfully for {}", loanDetails);

        return loanDetailsRespons;
    }

}
