package com.bank.plangenerator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class JsonInput<T, E> {
    private T obj;
    private E[] objarray;
}
