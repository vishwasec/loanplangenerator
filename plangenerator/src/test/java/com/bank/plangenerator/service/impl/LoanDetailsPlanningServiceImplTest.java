package com.bank.plangenerator.service.impl;

import com.bank.plangenerator.JsonInput;
import com.bank.plangenerator.controller.dto.LoanDetailsRequestDto;
import com.bank.plangenerator.controller.dto.LoanDetailsResponseDto;
import com.bank.plangenerator.domainobjects.LoanDetails;
import com.bank.plangenerator.domainobjects.PaymentHistory;
import com.bank.plangenerator.mapper.LoanDetailsMapper;
import com.bank.plangenerator.service.LoanDetailsPlanningService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.IntStream.range;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith(MockitoExtension.class)
class LoanDetailsPlanningServiceImplTest {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    private LoanDetailsPlanningService loanDetailsPlanningService = new LoanDetailsPlanningServiceImpl();

    private static List<JsonInput> buildLoanDto() {
        return range(1, 4).mapToObj(LoanDetailsPlanningServiceImplTest::getJsonInput)
                .collect(Collectors.toList());
    }

    private static JsonInput getJsonInput(int index) {
        File inputresource = new File("src/test/resources/input/test" + index + ".json");
        File outputresource = new File("src/test/resources/output/test" + index + ".json");

        LoanDetails loanDetails = null;
        LoanDetailsResponseDto[] loanDetailsResponseDto = null;
        try {
            loanDetails = LoanDetailsMapper.toLoanDetails(objectMapper.readValue(inputresource, LoanDetailsRequestDto.class));
            loanDetailsResponseDto = objectMapper.readValue(outputresource, LoanDetailsResponseDto[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return JsonInput.builder()
                .obj(loanDetails)
                .objarray(loanDetailsResponseDto).build();
    }

    @ParameterizedTest
    @MethodSource("buildLoanDto")
    void generatePlan(JsonInput<LoanDetails, LoanDetailsResponseDto> jsonInput) {
        List<PaymentHistory> paymentHistories = loanDetailsPlanningService.generatePlan(jsonInput.getObj());
        assertNotNull(paymentHistories);
        assertEquals(jsonInput.getObjarray().length, paymentHistories.size());
    }


}