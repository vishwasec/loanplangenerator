package com.bank.plangenerator.controller;

import com.bank.plangenerator.controller.dto.LoanDetailsRequestDto;
import com.bank.plangenerator.controller.dto.LoanDetailsResponseDto;
import com.bank.plangenerator.exceptions.InvalidInputException;
import com.bank.plangenerator.mapper.LoanDetailsMapper;
import com.bank.plangenerator.service.LoanDetailsPlanningService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.bank.plangenerator.controller.validator.LoanDetailsPlanningValidator.validate;
import static com.bank.plangenerator.mapper.LoanDetailsMapper.toLoanDetails;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("generate-plan")
@Api(description = "Loan planning api")
public class LoanDetailsPlanningController {

    private final LoanDetailsPlanningService loanDetailsPlanningService;

    @PostMapping
    @ApiOperation(
            value = "Returns list of loandetails every month.",
            response = List.class)
    @ApiResponses(
            value = {
                    @ApiResponse(code = 400, message = "When input params are not valid.")
            })

    public ResponseEntity<List<LoanDetailsResponseDto>> generatePlan(@RequestBody LoanDetailsRequestDto loanDetailsRequestDto) {

        log.info("Starting plan generation for {}", loanDetailsRequestDto);

        validate(loanDetailsRequestDto);

        return ok().body(loanDetailsPlanningService.generatePlan(toLoanDetails(loanDetailsRequestDto))
                .stream().map(LoanDetailsMapper::toLoanDetailsResponseDto)
                .collect(Collectors.toList()));
    }


    /**
     * handlePlanningException.
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidInputException.class)
    public String handlePlanningException(InvalidInputException ie) {

        log.info("handlePlanningException: {}", ie.getMessage());

        return ie.getMessage();
    }
}
