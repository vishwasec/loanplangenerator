package com.bank.plangenerator.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberUtilsTest {

    @Test
    void doubleToString() {
        assertEquals("20.0", NumberUtils.doubleToString(20.0));
    }

    @Test
    void round2() {
        assertEquals(20.34, NumberUtils.round2(20.3412121212, 2));
    }

    @Test
    void doubleLessThanZero() {
        assertEquals(4.44, NumberUtils.doubleLessThanZero(3.12, 2.22, 4.44));
    }

    @Test
    void numtoString() {
        assertEquals("12", NumberUtils.numtoString(12));
    }

    @Test
    void subDouble() {
        assertEquals(12.12, NumberUtils.subDouble(14.44, 2.32));

    }
}