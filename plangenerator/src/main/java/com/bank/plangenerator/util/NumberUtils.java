package com.bank.plangenerator.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.Math.abs;

public class NumberUtils {

    /**
     * Returns string of double with precision of 2.
     */
    public static String doubleToString(Double val) {

        return numtoString(round2(val, 2));
    }

    /**
     * Rounds to precision decimal value.
     */
    public static double round2(Double val, int precision) {

        return new BigDecimal(val.toString()).setScale(precision, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Based on the conditions returns retval or defaultVal
     */
    public static double doubleLessThanZero(double doublecond, double retval, double defaultval) {

        return (doublecond < 0) ? retval : defaultval;
    }

    /**
     * Returns null or the string value of the given object.
     */
    public static <T> String numtoString(T t) {

        return (t == null) ? null : String.valueOf(t);
    }

    /**
     * Subtract double with precision.
     */
    public static double subDouble(double a, double b) {
        return round2(a - abs(b), 2);
    }
}
