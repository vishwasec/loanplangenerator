package com.bank.plangenerator.controller.validator;

import com.bank.plangenerator.controller.dto.LoanDetailsRequestDto;
import com.bank.plangenerator.exceptions.InvalidInputException;

import java.time.format.DateTimeParseException;

import lombok.extern.slf4j.Slf4j;

import static java.lang.Double.parseDouble;
import static java.time.ZonedDateTime.parse;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
public class LoanDetailsPlanningValidator {

    public static void validate(LoanDetailsRequestDto loanDetailsRequestDto) {
        try {

            if (null == loanDetailsRequestDto || isEmpty(loanDetailsRequestDto.getDuration())
                    || isEmpty(loanDetailsRequestDto.getLoanAmount()) || isEmpty(loanDetailsRequestDto.getNominalRate())
                    || isEmpty(loanDetailsRequestDto.getStartDate())) {
                log.error("Missing required parameters.");
                throw new InvalidInputException("\"Missing required parameters.");
            }

            double amount = parseDouble(loanDetailsRequestDto.getLoanAmount());
            double rate = parseDouble(loanDetailsRequestDto.getNominalRate());
            parse(loanDetailsRequestDto.getStartDate(), ISO_DATE_TIME);

            if (amount <= 0 || loanDetailsRequestDto.getDuration() <= 0) {
                log.error("Amount = {}, duration = {}", amount, loanDetailsRequestDto.getDuration());
                throw new InvalidInputException("Amount or duration cant be 0");
            }

        } catch (NumberFormatException | DateTimeParseException e) {
            log.error("Failed to validate input, {}", e.getMessage());
            throw new InvalidInputException("Failed to validate input, " + e.getMessage());
        }
    }

}
