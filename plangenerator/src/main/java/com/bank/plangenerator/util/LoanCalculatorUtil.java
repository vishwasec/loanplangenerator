package com.bank.plangenerator.util;

import com.bank.plangenerator.domainobjects.LoanDetails;

import static com.bank.plangenerator.util.NumberUtils.round2;
import static java.lang.Math.pow;

public class LoanCalculatorUtil {
    private static final int DAYS_IN_YEAR = 360;
    private static final int DAYS_IN_MONTH = 30;
    private static final int PERCENTAGE = 100;
    private static final int MONTHS_IN_YEAR = 12;

    /**
     * Interest = (Rate * Days in Month * Initial Outstanding Principal) / Days
     * in Year e.g. first installment = (0.05 * 30 * 5000.00) / 360 = 20.83 €
     * (with rounding)
     */
    public static double getInterest(double rate, double loanamount) {

        return round2(rate / PERCENTAGE * DAYS_IN_MONTH * loanamount / DAYS_IN_YEAR, 2);
    }

    /**
     * The annuity amount has to be derived from three of the input parameters (duration, nominal interest rate, total loan amount) before starting the
     * plan calculation.
     * (use http://financeformulas.net/Annuity_Payment_Formula.html as reference)
     * Note : the nominal interest rate is an annual rate and must be converted to monthly before using in the annuity formula
     */
    private static double getAnnuity(double rate, double amount, double duration) {

        double monthlyRate = rate / MONTHS_IN_YEAR / PERCENTAGE;
        return round2(amount * monthlyRate / (1 - pow(1 + monthlyRate, -duration)), 2);
    }

    /**
     * Borrower Payment Amount(Annuity) = Principal + Interest e.g. first
     * borrower payment = 198.53 + 20.83 = 219.36
     */
    public static double getBorrowerPaymentAmount(LoanDetails loanDetails) {

        double interest = getInterest(loanDetails.getNominalRate(), loanDetails.getLoanAmount());
        double principal = getAnnuity(loanDetails.getNominalRate(), loanDetails.getLoanAmount(), loanDetails.getDuration()) - interest;
        return round2(principal + interest, 2);
    }

}
