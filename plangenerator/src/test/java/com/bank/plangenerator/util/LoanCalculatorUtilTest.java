package com.bank.plangenerator.util;

import com.bank.plangenerator.domainobjects.LoanDetails;

import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LoanCalculatorUtilTest {

    @Test
    void getInterest() {
        double interest = LoanCalculatorUtil.getInterest(50, 5000.0);
        assertEquals(208.33, interest);
    }

    @Test
    void getBorrowerPaymentAmount() {
        LoanDetails loanDetails = LoanDetails.builder()
                .duration(24)
                .loanAmount(2000.0)
                .nominalRate(5.0)
                .startDate(ZonedDateTime.now()).build();
        double payment = LoanCalculatorUtil.getBorrowerPaymentAmount(loanDetails);
        assertEquals(87.74, payment);
    }
}