package com.bank.plangenerator;

import com.bank.plangenerator.controller.LoanDetailsPlanningController;
import com.bank.plangenerator.controller.dto.LoanDetailsRequestDto;
import com.bank.plangenerator.controller.dto.LoanDetailsResponseDto;
import com.bank.plangenerator.service.LoanDetailsPlanningService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.IntStream.range;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("integration-test")
class PlangeneratorApplicationTests {

    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LoanDetailsPlanningController loanDetailsPlanningController;

    @Autowired
    private LoanDetailsPlanningService loanDetailsPlanningService;

    private static JsonInput getJsonInput(int index) {
        File inputresource = new File("src/test/resources/input/test" + index + ".json");
        File outputresource = new File("src/test/resources/output/test" + index + ".json");

        LoanDetailsRequestDto loanDetailsRequestDto = null;
        LoanDetailsResponseDto[] loanDetailsResponseDto = null;
        try {
            loanDetailsRequestDto = objectMapper.readValue(inputresource, LoanDetailsRequestDto.class);
            loanDetailsResponseDto = objectMapper.readValue(outputresource, LoanDetailsResponseDto[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return JsonInput.builder()
                .obj(loanDetailsRequestDto)
                .objarray(loanDetailsResponseDto).build();
    }

    @ParameterizedTest
    @MethodSource("buildLoanDto")
    public void successTest(JsonInput<LoanDetailsRequestDto, LoanDetailsResponseDto> jsonInput) throws Exception {

        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.post("/generate-plan", 42L)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(jsonInput.getObj())))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse();

        LoanDetailsResponseDto[] loanDetailsResponseDtos = getLoanResponseDto(response);
        assertNotNull(loanDetailsResponseDtos);
        assertTrue(Arrays.equals(jsonInput.getObjarray(), loanDetailsResponseDtos));
    }

    @Test
    public void failedTest() throws Exception {
        LoanDetailsRequestDto loanDetailsRequestDto = new LoanDetailsRequestDto();
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.post("/generate-plan", 42L)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(loanDetailsRequestDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse();

    }

    private static List<JsonInput> buildLoanDto() {
        return range(1, 4).mapToObj(PlangeneratorApplicationTests::getJsonInput)
                .collect(Collectors.toList());
    }

    private LoanDetailsResponseDto[] getLoanResponseDto(MockHttpServletResponse httpResponse) throws
            UnsupportedEncodingException, JsonProcessingException {
        return objectMapper.readValue(httpResponse.getContentAsString(), LoanDetailsResponseDto[].class);

    }


}
