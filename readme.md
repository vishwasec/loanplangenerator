# LoanPlanGenerator

**Plangenerator:** This app provides details of the amount the user needs to pay for the complete tenure. 

## Running

It can be run as an individual application or on cloud with all remaining services.

## To run locally,

```
cd plangenerator/

mvn spring-boot:run
```
**It can also be run as a docker. Use the ./runme.sh**
```
cd plangenerator/

./runme.sh dockerun     - dockerun : Starting as docker.[Currently doest work with gateway as docker localhost is different from eureka localhost.] 
./runme.sh bootrun                      - bootrun : Sarting as bootrun. mvnw spring-boot:run
./runme.sh stopdocker                   - stopdocker : Stop docker.
```

You can check actuator for status of app.

```
http://localhost:8080/actuator/info
http://localhost:8080/actuator/health
http://localhost:8080/actuator/metrics
```

## To run on cloud


To start eureka (client side load balancer.)
```
cd eureka/

mvn spring-boot:run
```

To start gateway (Zuul client, All the calls should be routed through gateway.)

```
cd gateway/

mvn spring-boot:run
```
To Start app plangenerator

```
cd plangenerator/

mvn spring-boot:run
```

Check eureka if all the services are up  **http://localhost:8761/**

**Note: as it takes few minutes for any app to register with eureka all the initials calls made through gateway would fail.**

---------Below information is also available at **./plangenerator/readme.md**----------

## About api's 

Once plangenerator is started you can check swagger for complete details and to try it directly.

Swagger url :  **http://localhost:8080/swagger-ui.html**

Or

Postman collection ./PlanGenerator.postman_collection.json

**POST /generate-plan**

Generates plan for the given input loandetails. Not the output is cached, so next call with the same would be much faster. TTL for cache is 10 mins, it can be configured 

To **call directly** to the service,
```
curl -X POST \
  http://localhost:8080/generate-plan \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8080' \
  -d '{
"loanAmount": "5000",
"nominalRate": "5.0",
"duration": 24,
"startDate": "2018-01-01T00:00:01Z"
}'

```

To call through **Gateway**, 

**Note initial calls would fail as it takes time for the services to register to eureka** 

```
curl -X POST \
  http://localhost:9300/generate-plan \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:9300' \
  -d '{
"loanAmount": "5000",
"nominalRate": "5.0",
"duration": 24,
"startDate": "2018-01-01T00:00:01Z"
}'

```

Payload
```
{
"loanAmount": "5000",
"nominalRate": "5.0",
"duration": 24,
"startDate": "2018-01-01T00:00:01Z"
}
```

Response
```
[
    {
        "borrowerPaymentAmount": "219.36",
        "date": "2018-01-01T00:00:01Z",
        "initialOutstandingPrincipal": "5000.0",
        "interest": "20.83",
        "principal": "198.53",
        "remainingOutstandingPrincipal": "4801.47"
    },
    
    ...
    
    {
            "borrowerPaymentAmount": "219.28",
            "date": "2019-12-01T00:00:01Z",
            "initialOutstandingPrincipal": "218.37",
            "interest": "0.91",
            "principal": "218.45",
            "remainingOutstandingPrincipal": "0.0"
        }
    ]
```

## Improvments 

- Authorization  service is missing, didnt get time to add this.

- Currently doesnt support to run the application as cloud if plangenerator is started as docker as docker localhost is different from eureka localhost.

- Docker for Eureka and Gateway. This would have simplified the startup process.

## Assumptions

- Ideally eureka and gateway should be in separate repositories for simplicity added it under a single repo.

