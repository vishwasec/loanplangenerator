package com.bank.plangenerator.controller.dto;


import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LoanDetailsRequestDto {
    @NotNull
    private String loanAmount;
    @NotNull
    private String nominalRate;
    @NotNull
    private Integer duration;
    @NotNull
    private String startDate;

}
